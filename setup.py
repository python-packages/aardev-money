from distutils.core import setup

setup(
    name='aardev-money',
    version='0.0.2',
    description='Money management api',
    author='Anton Raevsky',
    author_email='anton.raevsky.dev@gmail.com',
    url='',
    packages=['aardev_money'],
    classifiers=[
        'Programming Language :: Python :: 3.10'
    ]
)
