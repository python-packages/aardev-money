# Money operation package
#### Pypi
- https://pypi.org/project/aardev-money/

#### Roadmap
- add long description in pypi
- create list of supported currencies
- use list of supported currencies in Money class
- implement minus operation for money class
- edit error handlers
- create representation for money class
