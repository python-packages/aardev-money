import unittest
from _decimal import Decimal
from aardev_money import Money


class TestMoney(unittest.TestCase):

    def test_add_equal_currency(self) -> None:
        one_ruble = Money(1, 'RUB')
        two_ruble = Money(2, 'RUB')

        self.assertEqual(Decimal(3), one_ruble + two_ruble)

    def test_add_different_currency(self) -> None:
        one_ruble = Money(1, 'RUB')
        two_dollar = Money(2, 'USD')

        with self.assertRaises(Exception):
            one_ruble + two_dollar
